%{?nodejs_find_provides_and_requires}
%global enable_tests 0
Name:                nodejs-chalk
Version:             1.1.3
Release:             2
Summary:             Terminal string styling done right
License:             MIT
URL:                 https://github.com/chalk/chalk
Source0:             http://registry.npmjs.org/chalk/-/chalk-%{version}.tgz
Source1:             https://raw.githubusercontent.com/chalk/chalk/v%{version}/test.js
Source2:             https://raw.githubusercontent.com/chalk/chalk/v%{version}/license
Patch0:              chalk-fix-lack-of-dim.patch
Patch1:              chalk-fix-tests.patch
BuildArch:           noarch
ExclusiveArch:       %{nodejs_arches} noarch
BuildRequires:       nodejs-packaging npm(ansi-styles) npm(escape-string-regexp)
BuildRequires:       npm(has-ansi) >= 2.0.0 npm(has-color) npm(strip-ansi) npm(supports-color)
%if 0%{?enable_tests}
BuildRequires:       npm(mocha) npm(require-uncached) npm(semver)
%endif
%description
%{summary}.

%prep
%setup -q -n package
cp -p %{SOURCE1} .
cp -p %{SOURCE2} .
%patch0 -p1
%patch1 -p1
%nodejs_fixdep has-color
%nodejs_fixdep ansi-styles '^2.0.0'
%nodejs_fixdep strip-ansi
%nodejs_fixdep supports-color

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/chalk
cp -pr package.json index.js \
    %{buildroot}%{nodejs_sitelib}/chalk
%nodejs_symlink_deps

%check
%nodejs_symlink_deps --check
%{__nodejs} -e 'require("./")'
%if 0%{?enable_tests}
ln -s %{nodejs_sitelib}/tape node_modules/tape
%{_bindir}/mocha -R spec
%else
%{_bindir}/echo -e "\e[101m -=#=- Tests disabled -=#=- \e[0m"
%endif

%files
%{!?_licensedir:%global license %doc}
%doc *.md
%license license
%{nodejs_sitelib}/chalk

%changelog
* Thu Nov 26 2020 caodongxia <caodongxia@huawei.com> - 1.1.3-2
- Delete file nodejs-chalk.spec.old 

* Mon Aug 17 2020 Anan Fu <fuanan3@huawei.com> - 1.1.3-1
- Package init
